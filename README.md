# Systemy operacyjne. Implementacja konsoli linuksowej

Implementacja niektórych funkcjonalności konsoli linuksowej w ramach projektu na przedmiot Systemy Operacyjne.

__Autorzy:__ Michał Wątroba, Paweł Przeniczny

__Jezyk implementacji:__ Java

__Wymagania systemowe:__ Java Runtime Environment, Java Developement Kit (testowane na java version "1.6.0_27")

__Dokumentacja__: [https://bitbucket.org/ardath/ss2/doc/specyfikacja.md
](https://bitbucket.org/ardath/ss2/raw/master/doc/specyfikacja.md)
)


__Instalacja__: 

  `git clone https://ardath@bitbucket.org/ardath/ss2.git`

  `cd ss2`

  `make`


__Uruchamianie__: 

  `make run` - uruchom program korzystając z domyślnego pliku konfiguracyjnego /home/<USER>/.simpleshellrc

  `make test` - uruchom program korzystając z opcjonalnego pliku konfiguracyjnego ./simpleshellrc

  `make debug`- uruchom program z opcją -d (pokaż komunikaty diagnostyczne)
  

__Repozytorium:__ [https://bitbucket.org/ardath/ss2](https://bitbucket.org/ardath/ss2)

