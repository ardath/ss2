package agh.so.simpleshell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Przechowuje komendy użytkownika (uprzednio sparsowane przez klasę {@link UserInputParser}).
 * <p>
 * Metoda {@link #execute(Shell)} pozwala powłoce uruchomić komendę.
 * </p>
 * @author Michał Wątroba, Paweł Przeniczny
 * 
 * @param name
 *            komenda
 * @param parameters
 *            opcjonalne parametry komendy
 */
public class Command {

	public Command(String name, String parameters) {
		this.name = name;
		this.parameters = parameters;
	}

	public Command() {
	}

	/**
	 * Pozwala powłoce uruchomić komendę.
	 * <p>
	 * Standardowe wyjście i wyjście błędu są wyświetlane na ekranie.
	 * <p>
	 * Komenda wymaga przekazania instancji powłoki.
	 * </p>
	 * @param shell
	 * 			  instancja powłoki
	 */
	public void execute(Shell shell) throws IOException {
		// System.out.println(toString());
		Process p = Runtime.getRuntime()
				.exec(this.name + " " + this.parameters);

		BufferedReader stdOut = new BufferedReader(new InputStreamReader(
				p.getInputStream()));

		BufferedReader stdErr = new BufferedReader(new InputStreamReader(
				p.getErrorStream()));

		// read the output of the command
		String line;
		while ((line = stdOut.readLine()) != null) {
			System.out.println(line);
		}

		// read errors if any 
		while ((line = stdErr.readLine()) != null) {
			System.out.println("STDERR: " + line);
		}
	}

	/*
	 * Wyświetla komendę i jej parametry.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Command [" + (name != null ? "name=" + name + ", " : "")
				+ (parameters != null ? "parameters=" + parameters : "") + "]";
	}

	/**
	 * Przechowuje komendę. 
	 */
	private String name;
	/**
	 * Przechowuje opcjonalne parametry komendy. 
	 */
	private String parameters;

	
	/**
	 * Funkcja tylko dla testów.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
