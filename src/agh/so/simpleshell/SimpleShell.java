package agh.so.simpleshell;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.ParseException;

/**
 * Główna klasa. Uruchamia program SimpleShell.
 * <p>
 * Najpierw parsowane są argumenty linii poleceń.
 * <p>
 * Jeśli użytkownik wybrał opcje -v/--version lub -h/--help, pokazywana jest
 * wersja programu lub wyświetlana pomoc.
 * </p>
 * <p>
 * Użytkownik może także wybrać własny plik konfiguracyjny którym
 * inicjalizowana będzie powłoka. Domyślnie jest to plik w katalogu domowym
 * o nazwie '.simpleshellrc'
 * </p>
 * 
 * @author Michał Wątroba, Paweł Przeniczny
 * 
 * @param args
 *            Parametry zadane z linii poleceń.
 */
public class SimpleShell {
	/**
	 * Flaga ułatwiająca debugowanie programu
	 * 
	 * Jeśli DEBUG == true :   Pokaż wszystkie logi.
	 */
	public static boolean DEBUG = false;

	/**
	 * Główny logger programu - pokazuje komunikaty diagnostyczne.
	 * 
	 */
	public final static Logger LOGGER = Logger.getLogger(ConfigFileParser.class.getName());

	public static void main(String[] args) {

		// Stwórz loggera:
		final Logger LOGGER = Logger.getLogger(SimpleShell.class.getName());
		if (SimpleShell.DEBUG) LOGGER.log(Level.INFO, "Testowanie programu SimpleShell..");

		// nazwa pliku konfiguracyjnego:
		String configFileName = null;
		Shell shell;

		try {
			// parsuj argumenty linii poleceń:
			ShellCommandLineParser sCLIParser = new ShellCommandLineParser(args);

			if (sCLIParser.hasOption("d"))
				DEBUG = true;
			
			// stwórz 'pustą' powłokę, pokaż wersję i wyjdź z programu:
			if (sCLIParser.hasOption("v")) {
				System.out.println("Aktualna wersja programu: "
						+ new Shell().getVersion());
				System.exit(0);
			}

			// pokaż pomoc i wyjdź z programu:
			if (sCLIParser.hasOption("h")) {
				sCLIParser.showCLIHelp();
				System.exit(0);
			}

			// wczytaj zdefiniowany przez użytkownika plik z konfiguracją:
			if (sCLIParser.hasOption("f")) {
				configFileName = sCLIParser.getConfigFileName();
			} 

			// uruchom powłokę:
			shell = new Shell(configFileName);
			shell.run();

			if (DEBUG) LOGGER.log(Level.INFO, shell.toString());

		} catch (ParseException e) {
			System.err.println("Nie udało się sparsować konfiguracji. "
					+ e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
			
}