package agh.so.simpleshell;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.ConfigurationException;

/**
 * Klasa Shell.
 * <p>
 * Przechowuje instancję powłoki wraz z jej konfiguracją i metodami. 
 * 
 * @author Michał Wątroba, Paweł Przeniczny
 * 
 */

public class Shell {
	/**
	 * Pusty konstruktor, jedynie po to by pokazać wersję powłoki przy
	 * uruchamianiu programu z opcją '-v'.
	 */
	public Shell() {
	}

	/**
	 * Główny konstruktor. Inicjalizuje powłokę danymi pobranymi z pliku
	 * konfiguracyjnego.
	 * 
	 * @throws IOException
	 * @param configFileName	
	 * 					Ścieżka do pliku konfiguracyjnego
	 */
	public Shell(String configFileName) throws IOException {
		configuration = intializeConfiguration(configFileName);

		prompt = configuration.getPrompt();
		path = configuration.getDefaultPath();
		variables = configuration.getVariables();
		aliases = configuration.getAliases();
	}

	/**
	 * Funkcja pomocnicza. 
	 * <p>
	 * Inicjalizuje konfiguracje próbując wczytać ją z zadanego pliku.
	 * <p>
	 * Jeśli zadany plik nie istnieje, próbuje wczytać zmienne z pliku o domyślnej, predefiniowanej nazwie. 
	 * <p>
	 * Jeśli domyślny plik nie istnieje, wczytuje domyślną, minimalną (zapisaną w kodzie) konfigurację.
	 * 
	 * @param configFileName
	 * 			Zadana ścieżka do pliku z konfiguracją.
	 * @return
	 * 			Instancja konfiguracji (klasy {@link Configuration}).
	 * @throws IOException
	 */
	private Configuration intializeConfiguration(String configFileName)
			throws IOException {

		Configuration configuration;
		if (configFileName == null)
			configFileName = DEFAULT_CONFIG_FILE_PATH;
		File configurationFile = new File(configFileName);

		if (configurationFile.exists()) {
			if (SimpleShell.DEBUG) LOGGER.log(Level.INFO, "Plik konfiguracyjny istnieje. Parsowanie... ");
			configuration = ConfigFileParser.parse(configurationFile);
		} else {
			if (SimpleShell.DEBUG) LOGGER.log(Level.INFO, "Plik konfiguracyjny nie istnieje. Wczytywanie domyślnej konfiguracji. ");
			configuration = new DefaultConfiguration();
		}

		return configuration;
	}

	/**
	 * Uruchamia powłokę z zainicjalizowaną konfiguracją.
	 * <p>
	 * Wyświetla użytkownikowi znak zachęty (prompt) i czeka na wpisanie komendy.
	 * <p>
	 * Parsuje input użytkownika i uruchamia wpisaną komendę zwracając jej wynik i błędy na wyjście.
	 * <p>
	 * Następnie oczekuje na kolejną komendę itd.
	 */
	public void run() throws IOException {
		// getConfiguration().setVariable("LAST_CMD", "ls");

		/* Print prompt */
		if (SimpleShell.DEBUG) LOGGER.log(Level.INFO, "prompt := " + getPrompt());
		System.out.print(prompt + " ");

		Scanner scanner = new Scanner(System.in);
		String userInput;
		while (scanner.hasNextLine()) {
			userInput = scanner.nextLine();
			Command c = UserInputParser.parse(userInput, this);
			c.execute(this);

			System.out.print(prompt + " ");
		}
		CommandFactory.leave();

	};

	/**
	 * Zwraca zmienne i aliasy powłoki.
	 * 
	 * @return String
	 *				Zmienne i aliasy w formie gotowej do wyświetlenia na ekranie.
	 */
	public String toString() {
		String[] msg = {
				"Zmienne powłoki:",
				getVariables(),
				"Aliasy powłoki:",
				getAliases()
				/*
				 * + ", other variables: " + getVariables()
				 */
				};
		return Auxiliary.join(msg , "\n");
	}

	/**
	 * Zwraca aliasy powłoki.
	 * 
	 * @return String
	 *				Aliasy w formie gotowej do wyświetlenia na ekranie.
	 */
	public String getAliases() {
		StringBuilder builder = new StringBuilder();
	    Iterator it = configuration.getAliases().entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        builder.append(pairs.getKey() + " = " + pairs.getValue());
	        builder.append("\n");
		}
		return builder.toString();
	}

	/**
	 * Zwraca zmienne powłoki.
	 * 
	 * @return String
	 *				Zmienne w formie gotowej do wyświetlenia na ekranie.
	 */
	public String getVariables() {
		StringBuilder builder = new StringBuilder();
	    Iterator it = configuration.getVariables().entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        builder.append(pairs.getKey() + " = " + pairs.getValue());
	        builder.append("\n");
		}
		return builder.toString();
	}

	/**
	 * Wrapper na pole przechowujące konfigurację (klasy {@link Configuration})
	 * @return {@link Configuration}
	 * 						obiekt przechowujący konfigurację
	 */
	public static Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * Zwraca wartość zmiennej.
	 * Jeśli zmienna nie ma przypisanej wartości, 
	 * 
	 * @return String
	 * 				wartość zmiennej
	 */
	public String getValueOfTheVariable(String variable) {
		return configuration.getValueOfTheVariable(variable);
	}

	private String getPrompt() {
		return configuration.getValueOfTheVariable("PROMPT");
	}


	public void setPrompt(String prompt) {
		this.configuration.setVariable("PROMPT", prompt);
	}

	public String getVersion() {
		return VERSION;
	}

	/**
	 * przechowuje prompta
	 */
	private String prompt;

	/**
	 * przechowuje zmienne i aliasy powłoki
	 */
	private Map<String, String> variables;
	private Map<String, String> aliases;
	private String path; 

	private static Configuration configuration;


	/**
	 * Domyślna ścieżka do pliku konfiguracyjnego programu
	 */
	private final String DEFAULT_CONFIG_FILE_NAME = ".simpleshellrc";
	private final String DEFAULT_CONFIG_FILE_PATH = new File(
			System.getProperty("user.home"), DEFAULT_CONFIG_FILE_NAME).toString();

	/**
	 * Numer aktualnej wersji programu.
	 */
	private final static String VERSION = "1.5";

	public static final Logger LOGGER = Logger.getLogger(Shell.class.getName());


	/**
	 * Różne testy klasy Shell.
	 * 
	 * @param args
	 */

	/* Nie zaimplementowane..
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		if (SimpleShell.DEBUG)
			LOGGER.log(Level.INFO, "Testowanie klasy Shell");
		// Shell sh = new Shell("/home/ardath/.simplesellrc");
		// Logger.getLogger("simpleshell").log(Level.INFO,
		// "shell as string: " + sh.toString());

		try {
			boolean color = false;
			char mask = 0;
			String trigger = null;

			ConsoleReader reader = new ConsoleReader();
			reader.setPrompt("PROMPT_");
			if ((args == null) || (args.length == 0)) {
				usage();

				return;
			}

			List<Completer> completors = new LinkedList<Completer>();

			if (args.length > 0) {
				if (args[0].equals("none")) {
				} else if (args[0].equals("files")) {
					completors.add(new FileNameCompleter());
				} else if (args[0].equals("simple")) {
					completors.add(new StringsCompleter("foo", "bar", "baz"));
					if (SimpleShell.DEBUG) LOGGER.log(Level.INFO, "simple completor");
				} else if (args[0].equals("color")) {
					color = true;
					reader.setPrompt("\u001B[1mfoo\u001B[0m@bar\u001B[32m@baz\u001B[0m> ");
				} else {
					usage();

					return;
				}
			}

			if (args.length == 3) {
				mask = args[2].charAt(0);
				trigger = args[1];
			}

			for (Completer c : completors) {
				reader.addCompleter(c);
			}

			String line;
			PrintWriter out = new PrintWriter(reader.getOutput());

			while ((line = reader.readLine()) != null) {
				if (color) {
					out.println("\u001B[33m======>\u001B[0m\"" + line + "\"");

				} else {
					out.println("======>\"" + line + "\"");
				}
				out.flush();

				// If we input the special word then we will mask
				// the next line.
				if ((trigger != null) && (line.compareTo(trigger) == 0)) {
					line = reader.readLine("password> ", mask);
				}
				if (line.equalsIgnoreCase("quit")
						|| line.equalsIgnoreCase("exit")) {
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				TerminalFactory.get().restore();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static void usage() {
		System.out.println("Usage: java " + Shell.class.getName()
				+ " [none/simple/files/dictionary [trigger mask]]");
		System.out.println("  none - no completors");
		System.out.println("  simple - a simple completor that comples "
				+ "\"foo\", \"bar\", and \"baz\"");
		System.out
				.println("  files - a completor that comples " + "file names");
		System.out.println("  classes - a completor that comples "
				+ "java class names");
		System.out
				.println("  trigger - a special word which causes it to assume "
						+ "the next line is a password");
		System.out.println("  mask - is the character to print in place of "
				+ "the actual password character");
		System.out.println("  color - colored prompt and feedback");
		System.out.println("\n  E.g - java Example simple su '*'\n"
				+ "will use the simple compleator with 'su' triggering\n"
				+ "the use of '*' as a password mask.");
	}
	*/

}