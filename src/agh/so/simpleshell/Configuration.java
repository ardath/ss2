package agh.so.simpleshell;

import java.util.Map;

/**
 * Wspólny interfejs dla obiektów przechowujących konfigurację.
 * 
 * @author Michał Wątroba, Paweł Przeniczny.
 */
public interface Configuration {
	public String getPrompt();
	public String getValueOfTheVariable(String variable);
	public String getDefinitionOfAnAlias(String alias);
	public Map<String, String> getVariables();
	public Map<String, String> getAliases();
//	public String toString();
	public String getDefaultPath();
	public void setVariable(String string, String value);
}
