package agh.so.simpleshell;

import java.util.HashMap;
import java.util.Map;

/**
 * Domyślna konfiguracja, 
 * <p>
 * Wykorzystywana gdy brak pliku konfiguracyjnego lub brak wymaganych zmiennych w zadanym pliku.
 *  
 * @author Michał Wątroba, Paweł Przeniczny
 *
 */
public class DefaultConfiguration implements Configuration {

	/**
	 * Inicjalizuje obiekt konfiguracji domyśnymi wartościami zmiennych niezbędnych dla działania programu.
	 */
	public DefaultConfiguration() {
		this.variables = new HashMap<String, String>();
		this.aliases = new HashMap<String, String>();
		variables.put("PROMPT", DEFAULT_PROMPT);
		variables.put("PATH", DEFAULT_PATH);
	}

	@Override
	/**
	 * Zwraca prompt.
	 */
	public String getPrompt() {
		return getValueOfTheVariable("PROMPT");
	}

	@Override
	/**
	 * Zwraca wartość zmiennej jeśli jest ona zdefiniowana.
	 */
	public String getValueOfTheVariable(String variable) {
		// TODO Auto-generated method stub
		if (variables.get(variable) != null) return variables.get(variable);
		return "";
	}

	@Override
	/**
	 * Zwraca definicję aliasu.
	 */
	public String getDefinitionOfAnAlias(String alias) {
		return aliases.get(alias);
	}

	@Override
	/**
	 * Zwraca obiekt przechowujący zmienne i ich wartości. 
	 */
	public Map<String, String> getVariables() {
		// TODO Auto-generated method stub
		return variables;
	}

	@Override
	/**
	 * Zwraca obiekt przechowujący aliasy i ich definicje. 
	 */
	public Map<String, String> getAliases() {
		// TODO Auto-generated method stub
		return aliases;
	}

	/**
	 * Zwraca domyślną wartość zmiennej $PATH.
	 */
	public String getDefaultPath() {
		return DEFAULT_PATH;
	}

	/**
	 * Przechowuje zmienne i ich wartości.
	 */
	private Map<String, String> variables;

	/**
	 * Przechowuje aliasy i ich definicje.
	 */
	private Map<String, String> aliases;

	private String prompt;
	private String path;

	/**
	 * Zmienne przechowujące domyślne wartości niezbędnych do działania zmiennych powłoki.
	 */
	private String DEFAULT_PROMPT = "$";
	/**
	 * Zmienne przechowujące domyślne wartości niezbędnych do działania zmiennych powłoki.
	 */
	private String DEFAULT_PATH = "/usr/bin:/bin:/usr/local/bin";

	@Override
	// nie zaimplementowane..
	public void setVariable(String string, String value) {
		variables.put(string, value);
	}

	// nie zaimplementowane..
	public void setAlias(String string, String value) {
		aliases.put(string, value);
	}
	
	/**
	 * Testy..
	 * @param args
	 */

	public static void main(String[] args) {
		DefaultConfiguration defaultConfiguration = new DefaultConfiguration();
		System.out.println(defaultConfiguration.getPrompt());
	}
}
