package agh.so.simpleshell;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

/**
 * Parsuje argumenty linii poleceń.
 * <p>
 * Jeśli użytkownik wybrał opcje -v/--version lub -h/--help, pokazywana jest
 * wersja programu lub wyświetlana pomoc.
 * </p>
 * Jeśli użytkownik wybrał opcje -d/--debug, wyświetlane są na ekranie komunikaty diagnostyczne pomocne w debugowaniu programu.
 * </p>
 * <p>
 * Użytkownik może także wybrać własny plik konfiguracyjny którym
 * inicjalizowana będzie powłoka. Domyślnie jest to plik w katalogu domowym
 * o nazwie '.simpleshellrc'
 * </p>
 * 
 * @author Michał Wątroba, Paweł Przeniczny
 * 
 * @param args
 *            Parametry zadane z linii poleceń.
 */
public class ShellCommandLineParser {


	public ShellCommandLineParser(String[] args)
			throws org.apache.commons.cli.ParseException {

		commandLineOptions = new Options()
				.addOption("f", "file", true,
						"Załaduj własny plik konfiguracyjny")
				.addOption("v", "version", false,
						"Pokaż numer wersji programu.")
				.addOption("h", "help", false,
						"Pokaż jak korzystać z programu.")
				.addOption("d", "debug", false,
						"Pomoc w debugowaniu (pokaż log).")
				;

		CommandLineParser cliParser = new PosixParser();
		cmd = cliParser.parse(commandLineOptions, args);

		if (cmd.hasOption("-f")) {
			assignConfigFileName(cmd.getOptionValue("f"));
		}
	}

	/**
	 * Zwraca ścieżkę do pliku konfiguracyjnego.
	 * 
	 * @return String nazwa pliku
	 */
	public String getConfigFileName() {
		return configFileName;
	}

	/**
	 * Sprawdza czy wśród argumentów linii poleceń znalazła się zadana opcja.
	 * 
	 * @param o	jedna z predefiniowanych opcji: [ file | version | help | debug ]
	 * @return boolean
	 */
	public boolean hasOption(String o) {
		return cmd.hasOption(o);
	}

	/**
	 * Pokazuje komunikat pomocy dotyczący uruchamiania programu.
	 * 
	 */
	public void showCLIHelp() {
		if (SimpleShell.DEBUG) {
			LOGGER.log(Level.INFO, "Program uruchomiony z opcją '-h'.");
		}
		System.out.println("Pomoc w korzystaniu z programu: ");
		HelpFormatter hf = new HelpFormatter();
		hf.printHelp("SimpleShell", commandLineOptions);
		System.out.println();
		System.exit(0);
	}

	/**
	 * Przypisuje polu zawierającemu ścieżkę pliku konfiguracyjnego zadaną wartość. 
	 * 
	 * @param String ścieżka do pliku
	 */
	private void assignConfigFileName(String fileName) {
		if (SimpleShell.DEBUG) {
			LOGGER.log(Level.INFO, "Program uruchomiony z opcją '-f'.");
		}
		configFileName = fileName;
		if (SimpleShell.DEBUG) {
			LOGGER.log(Level.INFO, "Parametr: '" + configFileName + "'.");
		}
	}

	/**
	 * Przechowuje zadane przez użytkownika parametry linii poleceń programu.
	 */
	private final CommandLine cmd;

	/**
	 * Przechowuje ścieżkę do pliku konfiguracyjnego.
	 */
	private String configFileName;

	/**
	 * Przechowuje zaimplementowane parametry linii poleceń programu.
	 */
	private final Options commandLineOptions;

	/**
	 * Logger dla testów klasy.
	 */
	private final static Logger LOGGER = Logger
			.getLogger(ShellCommandLineParser.class.getName());

	/**
	 * Funkcja main dla testów klasy.
	 * 
	 * @param args
	 * @throws org.apache.commons.cli.ParseException
	 */
	public static void main(String[] args)
			throws org.apache.commons.cli.ParseException {
		// TODO Auto-generated method stub
		ShellCommandLineParser sclip = new ShellCommandLineParser(args);
		String f = sclip.getConfigFileName();
		LOGGER.log(Level.INFO, f);
	}

}
