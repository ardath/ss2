package agh.so.simpleshell;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.common.collect.Maps;

/**
 * Przechowuje parametry konfiguracyjne programu:
 * <p> 
 * - zmienne powłoki, w tym zmienne specjalne: $PROMPT, $PATH i zmienne zdefiniowane przez użytkownika,
 * <p>
 * - aliasy zdefiniowane przez użytkownika.
 * 
 * @author Michał Wątroba, Paweł Przeniczny
 * 
 */
public class ConfigurationFromFile<V> extends DefaultConfiguration implements
		Configuration {
	public static final boolean DEBUG = SimpleShell.DEBUG;


	/**
	 * Konstruktor uruchomiający konstruktor rodzica.
	 * 
	 */
	public ConfigurationFromFile() {
		super();
	};

	/**
	 * Konstruktor inicjalizujący aliasy i zmienne obiektami wczytanymi z plików konfiguracyjnych.
	 */
	public ConfigurationFromFile(Map<String, String> theVariables,
			Map<String, String> theAliases) {
		super();
		variables = theVariables;
		aliases = theAliases;
	}

	@Override
	/**
	 * Zwraca znak zachęty.
	 * @return String 
	 * 			Znak zachęty.
	 */
	public String getPrompt() {
		String prompt = getValueOfTheVariable("PROMPT");
		if (SimpleShell.DEBUG)
			LOGGER.log(Level.INFO, "PROMPT := "+prompt);

		if (prompt == null) prompt = new DefaultConfiguration().getPrompt();

		if (SimpleShell.DEBUG)
			LOGGER.log(Level.INFO, "PROMPT := "+prompt);
		return prompt;
	}

	/**
	 * Zwraca wartość zmiennej $PATH.
	 * @return String 
	 * 			Wartość $PATH
	 */
	public String getPath() {
		String path = getValueOfTheVariable("PATH");
		if (path == null)
			path = new DefaultConfiguration().getDefaultPath();
		return path;
	}

	@Override
	/**
	 * Zwraca definicję aliasa.
	 * 
	 * @return String 
	 * 			Definicja aliasa.
	 */
	public String getDefinitionOfAnAlias(String alias) {
		return aliases.get(alias);
	}

	@Override
	/**
	 * Zwraca wartość zmiennej powłoki.
	 * <p>
	 * Jeśli zmienna nie istnieje, zwraca pusty ciąg znaków.
	 * 
	 * @return String 
	 * 			Wartość zmiennej.
	 */
	public String getValueOfTheVariable(String variable) {
		Map<String, String> vs = getVariables();
		String value = vs.get(variable);
		if (SimpleShell.DEBUG)
			LOGGER.log(Level.INFO, variable+" := "+value);

		if (value == null) value = "";

		if (SimpleShell.DEBUG)
			LOGGER.log(Level.INFO, variable+" := "+value);
		return value;
//		return getVariables().get(variable);
	}

	@Override
	/** 
	 * Zwraca obiekt przechowujący wszystkie zmienne i ich wartości.
	 * 
	 * @return Map<String, String>
	 * 
	 */
	public Map<String, String> getVariables() {
		return variables;
	}

	@Override
	/** 
	 * Zwraca obiekt przechowujący wszystkie aliasy i ich definicje.
	 * 
	 * @return Map<String, String>
	 * 
	 */
	public Map<String, String> getAliases() {
		return aliases;
	}

	/** 
	 * Obiekt przechowujący wszystkie zmienne i ich wartości.
	 */
	private Map<String, String> variables;

	/** 
	 * Obiekt przechowujący wszystkie aliasy i ich definicje.
	 */
	private Map<String, String> aliases;

	/**
	 * Logger dla testów.
	 */
	private final static Logger LOGGER = Logger
			.getLogger(ConfigurationFromFile.class.getName());

	/**
	 * funkcja main
	 * 
	 * Stworzona na potrzeby testów klasy.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) {
		if (SimpleShell.DEBUG) LOGGER.log(Level.INFO, "Testuję klasę Configuration.. ");

		Configuration c = new ConfigurationFromFile(null, null);
		// LOGGER.log(Level.INFO, c.getVariables().toString());
		LOGGER.log(Level.INFO, c.getPrompt());
	}

}
