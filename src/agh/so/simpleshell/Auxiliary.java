package agh.so.simpleshell;

import java.lang.reflect.Array;

/**
 * Klasa z pomocniczymi funkcjami które nie pasują do innych klas lub których import wymagał by dołączania dodatkowych bibliotek..
 * 
 * @author Michał Wątroba, Paweł Przeniczny, stackoverflow.com
 *
 */
public class Auxiliary {

	/**
	 * Połącz tablicę stringów przy pomocy separatora.
	 * 
	 * @param r 
	 * 			tablica stringów
	 * @param d
	 * 			separator
	 * @return
	 * 			Połączony ciąg znaków.
	 */
	public static String join(String r[], String d) {
		if (r.length == 0)
			return "";
		StringBuilder sb = new StringBuilder();
		int i;
		for (i = 0; i < r.length - 1; i++)
			sb.append(r[i] + d);
		return sb.toString() + r[i];
	}
	

	/**
	 * Dodaj dwie tablice i zwróć wynik.
	 * 
	 * @param A tablica
	 * @param B tablica
	 * @return tablicaA+tablicaB
	 */
	public static <T> T[] concatenate(T[] A, T[] B) {
		int aLen = A.length;
		int bLen = B.length;

		@SuppressWarnings("unchecked")
		T[] C = (T[]) Array.newInstance(A.getClass().getComponentType(), aLen
				+ bLen);
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);

		return C;
	}
}
