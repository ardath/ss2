package agh.so.simpleshell;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parsuje zadany plik z konfiguracją i zwraca obiekt przechowujący konfigurację.
 * <p>
 * Jeśli plik zawiera poprawne definicje zmiennych i/lub aliasów, są one dodawane do konfiguracji.
 * 
 * @author Michał Wątroba, Paweł Przeniczny
 *
 */
public class ConfigFileParser {

	/**
	 * Metoda statyczna odpowiadająca za parsowanie pliku.
	 * <p>
	 * Parsuje zadany plik linia po linii w poszukiwaniu definicji zmiennych lub aliasów. 
	 * <p>
	 * Jeśli odnajdzie poprawną definicję, dodaje ją do odpowiedniego kontenera - osobno dla aliasów, osobno dla zmiennych.
	 * 
	 * @param aFile
	 * 			Plik z konfiguracją.
	 * @return
	 * 		  Obiekt zawierający konfigurację powłoki (implementujący interfejs {@link Configuration}.
	 * 
	 * @throws IOException
	 */
	public static ConfigurationFromFile parse(File aFile) throws IOException {
		// Configuration

		Map<String, String> variables = new HashMap<String, String>();
		Map<String, String> aliases = new HashMap<String, String>();

		LineNumberReader br = new LineNumberReader(new FileReader(aFile));

		try {
			// wczytaj plik linia po linii:
			String line = br.readLine();
			while (line != null) {
				// pomiń puste linie:
				if (isEmpty(line)) {
					if (SimpleShell.DEBUG) LOGGER.log( Level.INFO, "Linia nr " + br.getLineNumber() +  " jest pusta. Wczytuję następną linię. ");
					line = br.readLine();
					continue;
				}
				// dodaj alias i jego definicję lub zmienną i jej wartość do odpowiedniego kontenera:
				if (isCorrectConfigDefinition(line)) {
					if (SimpleShell.DEBUG) LOGGER.log( Level.INFO, "Linia nr " + br.getLineNumber() +  " (\'" + line
									+ "\') wygląda na poprawną definicję w pliku konfiguracyjnym. Próbuję parsować jej wartość. ");

					Map<String, String> container = variables;
					String[] definition = line
							.split(CONFIG_KEY_VALUE_SEPARATOR);
					String key = definition[0].trim();
					String value = definition[1].trim().replace("\"", "");

					if (key.startsWith("alias")) {
						key = key.split(" ")[1];
						container = aliases;
					}

					container.put(key, value);
				} else {
					// jeśli dana linia nie jest poprawną definicją, pomiń ją i wyświetl ostrzeżenie:
					String INCORRECT_CONFIG_LINE_WARNING = "Linia nr " +br.getLineNumber() +  " (\'" + line + "\') nie wygląda na poprawną definicję w pliku konfiguracyjnym i zostanie pominięta. ";
					if (SimpleShell.DEBUG) LOGGER.log( Level.INFO, INCORRECT_CONFIG_LINE_WARNING);
					System.out.println(INCORRECT_CONFIG_LINE_WARNING);
				}
				line = br.readLine();
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			br.close();
		}

		// jeśli w pliku nie były zdefiniowane zmienne PROMPT i PATH, wczytaj je z domyślnej konfiguracji:
		if (variables.get("PROMPT") == null) {
			DefaultConfiguration defaultConfiguration = new DefaultConfiguration();
			String prompt = defaultConfiguration.getPrompt();
			variables.put("PROMPT", prompt);
		}

		if (variables.get("PATH") == null) 
			variables.put("PATH", new DefaultConfiguration().getDefaultPath());

		// zwróć obiekt przechowujący konfigurację:
		return new ConfigurationFromFile(variables, aliases);
	}

	/**
	 * Sprawdza czy linia nie składa się wyłącznie ze spacji.
	 */
	private static boolean isEmpty(String line) {
		return line.matches("\\s*");
	}

	/**
	 * Sprawdza czy linia jest poprawną definicją aliasu lub zmiennej:
	 */
	private static boolean isCorrectConfigDefinition(String line) {
		return line.matches("(alias\\s)?\\w+\\s?=\\s?.+");
	}

	/**
	 * Przechowuje obiekt z konfiguracją.
	 */
	private static ConfigurationFromFile configurationFromFile;

	/**
	 * Domyślny separator kluczy i wartości w pliku konfiguracyjnym.
	 */
	private static final String CONFIG_KEY_VALUE_SEPARATOR = "=";

	/**
	 * Logger dla testów.
	 */
	public final static Logger LOGGER = Logger.getLogger(ConfigFileParser.class
			.getName());

	/**
	 * Dla testów..
	 * @param args
	 */
	public static void main(String[] args) {
		// debuguj tylko tę klasę:
		boolean DEBUG = true;

		// debuguj główny program:
		// DEBUG = SimpleShell.DEBUG;

		ConfigurationFromFile c;
		try {
			c = ConfigFileParser.parse(new File("/home/ardath/.simpleshellrc"));
			LOGGER.log(Level.INFO, "prompt: " + c.getPrompt());
			LOGGER.log(Level.INFO, "variables: " + c.getVariables().toString());
			LOGGER.log(Level.INFO, "aliases: " + c.getAliases().toString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
