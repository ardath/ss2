package agh.so.simpleshell;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Parsuje ciąg znaków wpisany przez użytkownika po znaku zachęty w poszukiwaniu
 * zmiennych, aliasów i poleceń.
 * 
 * @author Michał Wątroba, Paweł Przeniczny
 * 
 */
public class UserInputParser {

	/**
	 * Parsuje ciąg znaków wpisany przez użytkownika po znaku zachęty w
	 * poszukiwaniu zmiennych, aliasów i poleceń.
	 * <p>
	 * Usuwa białe znaki z ciągu wprowadzonego przez użytkownika.
	 * <p>
	 * Jeśli pierwszym poleceniem jest alias, zastępuje go jego definicją.
	 * <p>
	 * Jeśli napotka zmienną, zastępuje ją jej wartością.
	 * <p>
	 * Przekazuje sparsowany ciąg znaków fabryce komend klasy {@link CommandFactory} w celu utworzenia komendy.
	 * 
	 * @param input
	 * 			Ciąg znaków wprowadzonych przez użytkownika.
	 * @param shell 
	 * 			Instancja powłoki.
	 *
	 * @return Obiekt przechowujący komendę gotową do uruchomienia.
	 * 
	 */
	public static Command parse(String input, Shell shell) {
		if (SimpleShell.DEBUG)
			SimpleShell.LOGGER.log(Level.INFO, "Wprowadzono '" + input + "'.");

		String[] trimmedInput = input.trim().split("\\s+");
		if (SimpleShell.DEBUG)
			SimpleShell.LOGGER.log(Level.INFO, "trimmedInput '" + input + "'.");

		String command = trimmedInput[0];
		String[] parameters = Arrays.copyOfRange(trimmedInput, 1,
				trimmedInput.length);

		if (isAnAlias(command, shell)) {
			if (SimpleShell.DEBUG)
				SimpleShell.LOGGER.log(Level.INFO, "command '" + command
						+ "' is an alias.");
			String aliasedCommand = expandAlias(shell, command);
			String[] trimmedCommand = aliasedCommand.trim().split("\\s+");
			command = trimmedCommand[0];
			String[] trimmedParameters = Arrays.copyOfRange(trimmedCommand, 1,
					trimmedCommand.length);
			parameters = Auxiliary.concatenate(trimmedParameters, parameters);
		}

		for (int i = 0, l = parameters.length; i < l; i++) {
			if (isAVariable(parameters[i])) {

				if (SimpleShell.DEBUG)
					SimpleShell.LOGGER.log(Level.INFO, "variable found: "
							+ parameters[i]);

				parameters[i] = shell.getConfiguration().getValueOfTheVariable(
						parameters[i].substring(1));
			}
		}

		if (SimpleShell.DEBUG)
			SimpleShell.LOGGER.log(Level.INFO,
					"parameters: " + Auxiliary.join(parameters, ","));

		return CommandFactory.createCommand(command,
				Auxiliary.join(parameters, " "), shell);
	}

	private static boolean isAVariable(String string) {
		return string.matches("\\$\\w+");
	}

	private static boolean isAnAlias(String command, Shell shell) {
		Configuration config = shell.getConfiguration();
		if (config.getAliases() == null) {
			return false;
		} else {
			return config.getDefinitionOfAnAlias(command) != null;
		}
	}

	private static String expandAlias(Shell shell, String command) {
		return shell.getConfiguration().getDefinitionOfAnAlias(command);
	}


	public static void main(String[] args) throws IOException {
		System.out.println(UserInputParser.parse("find $v in $string  ",
				new Shell("/home/ardath/dev/ss.rc")));
		// System.out.println(UserInputParser.parse("     ls -l  ", new
		// Shell()));
		// System.out.println(UserInputParser.parse("ls   -l +10 / 2  ", new
		// Shell()));
	}
}
