package agh.so.simpleshell;

import java.io.File;
import java.util.logging.Level;

/**
 * Fabryka produkująca komendy konsoli na podstawie inputu użytkownika.
 * <p>
 * Zwraca instancje klasy {@link Command}.
 * <p>
 * Jeśli użytkownik uruchomi jedną z predefiniowanych komend (exit, ver, env), fabryka tworzy anonimową instancję klasy {@link Command} z przypisaną odpowiednią dla tej komendy akcją.
 * <p>
 * Jeśli wpisana komenda nie jest jednym z predefiniowanych poleceń, 
 * klasa sprawdza czy polecenie(aplikacja) znajduje się w systemie. 
 * <p>
 * Jeśli wynik testu jest pozytywny, aplikacja jest uruchamiana. 
 * <p>
 * W przeciwnym wypadku wyświetony zostaje komunikat o braku żądanej aplikacji. 
 * 
 * @author Michał Wątroba, Paweł Przeniczny
 *
 */
public class CommandFactory {
	/**
	 * 
	 * @param command
	 * 			Komenda zadana przez użytkownika.
	 * @param parameters
	 * 			Opcjonalne parametry komendy.
	 * @param shell
	 * 			Instancja powłoki (klasy {@link Shell}).
	 * @return
	 * 			Instancja klasy {@link Command} gotowa do uruchomienia.
	 */
	public static Command createCommand(String command, String parameters,
			Shell shell) {
		if (SimpleShell.DEBUG) {
			SimpleShell.LOGGER.log(Level.INFO, "command := " + command);
		}

		// predefiniowana komenda
		if (command.equals("help")) {
			shell.getConfiguration().setVariable("LAST_CMD", "help");
			return new Command() {
				public void execute(Shell shell) {
					showPredefinedCommands();
				}

				private void showPredefinedCommands() {
					String[] msg = {
							"Dostępne wewnętrzne komendy: ",
							"help - pokazuje pomoc",
							"env  - pokazuje dostępne zmienne i aliasy",
							"ver  - pokazuje aktualną wersję programu",
							"exit - zamyka program"
					};
					System.out.println(Auxiliary.join(msg, "\n"));
				}
			};
		} 

		// predefiniowana komenda
		if (command.equals("exit"))
			// shell.getConfiguration().setVariable("LAST_CMD", "exit");
			return new Command() {
				public void execute(Shell shell) {
					leave();
				}
			}; 

		// predefiniowana komenda
		if (command.equals("ver")) {
			shell.getConfiguration().setVariable("LAST_CMD", "ver");
			return new Command() {
				public void execute(Shell shell) {
					showVersion(shell);
				}
			};
		}

		// predefiniowana komenda
		if (command.equals("env"))
			return new Command() {
				public void execute(Shell shell) {
					printVariables(shell);
				}
			};

		// uruchom jeśli nie znaleziono w systemie żądanego polecenia 
		if (inPath(command, shell) == null) {
			shell.getConfiguration().setVariable("LAST_CMD", command);
			return new Command() {
				public void execute(Shell shell) {
					showNoSuchAppMsg(shell);
					//offerInstaller();
				}

				private void offerInstaller() {
					// TODO: not yet implemented..
					String os = System.getProperty("os.name");
					System.out.println(os);

				}

				private void showNoSuchAppMsg(Shell shell) {
					System.out.println("Brak programu lub komendy '"
							+ shell.getConfiguration().getValueOfTheVariable(
									"LAST_CMD") + "' w systemie.");
				}

			};
		}

		// uruchom jeśli polecenie istnieje  
		shell.getConfiguration().setVariable("LAST_CMD", command + " " + parameters);
		return new Command(command, parameters);

	}


	/** 
	 * Wychodzi z programu.
	 * 
	 */
	public static void leave() {
		System.out.println("Wyjście z programu.");
		System.exit(0);
	}

	/** 
	 * Testowanie...
	 * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/** 
	 * Pokazuje zdefiniowane zmienne i aliasy.
	 * 
	 * @param shell
	 * 			Instancja powłoki.
	 */
	public static void printVariables(Shell shell) {
		System.out.println(shell.toString());
	}

	/** 
	 * Pokazuje aktualną wersję programu.
	 * 
	 * @param shell
	 * 			Instancja powłoki.
	 */
	public static void showVersion(Shell shell) {
		System.out.println("Aktualna wersja programu: " + shell.getVersion());
	}


	/**
	 * Sprawdza czy aplikacja jest zainstalowana, tj. czy można ją odszukać w katalogach zdefiniowanych w zmiennej $PATH
	 * @param command
	 * 				Nazwa aplikacji
	 * @param shell
	 * 				Instancja powłoki (klasy {@link Shell})
	 * @return
	 * 		boolean
	 */
	private static String inPath(String command, Shell shell) {
		File fullPath = null;

		String[] paths = shell.getValueOfTheVariable("PATH").split(":");
		for (String path : paths) {
			if (SimpleShell.DEBUG) {
				SimpleShell.LOGGER.log(Level.INFO, "path := " + path);
			}

			fullPath = new File(path, command);
			if (fullPath.exists())
				return fullPath.toString();
		}

		return null;
	}

}
