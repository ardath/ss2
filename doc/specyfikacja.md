# Systemy operacyjne. Implementacja konsoli linuksowej

__Funkcjonalnosci__:
- zmienne:  
  `PATH` - string z lista katalogow w ktorych zainstalowane programy; nazwy katalogow oddzielone dwukropkami  
  `PROMPT`  - string z promptem  

- wbudowane polecenia:  

    `env`  - wyświetla listę zmiennych i aliasów powłoki

    `ver`  - wyświetla wersję programu

    `exit` - wychodzi z programu

- plik konfiguracyjny w ktorym mozna ustawiac zmienne i definiowac aliasy
