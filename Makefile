# Makefile for SimpleShell
#
# authors: Michał Wątroba, Paweł Przeniczny


PKG=SimpleShell.jar

JAVAC=javac
JAVA=java
JAR=jar


install: compile jar

run: 
	$(JAVA) -jar $(PKG)

test:
	$(JAVA) -jar $(PKG) -f simpleshellrc

debug: 
	$(JAVA) -jar $(PKG) -d


compile:
	-mkdir bin
	$(JAVAC) -d bin \
		-cp src/agh/so/simpleshell/:lib/commons-cli-1.2.jar \
		src/agh/so/simpleshell/*.java \


jar:
	$(JAR) cfm $(PKG) \
		META-INF/MANIFEST.MF \
		bin/agh/so/simpleshell/*.class


clean:
	rm -rf $(PKG)
	rm -rf bin/agh/so/simpleshell/*.class


.PHONY: clean
